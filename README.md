# White Canvas Challenge

## Requirements

To build this plugin you wil need:
- Composer
- Node + npm

## Build process

```
cd existing_repo
composer install
npm install
npm run build
```

## Installation

- Place the plugin folder on your plugins directory, usualy `wp-content/plugins`
- Activate the plugin going to "Plugins" on your wordpress admin dashboard
- Visit `/team-members/` URL to see the plugin's page
