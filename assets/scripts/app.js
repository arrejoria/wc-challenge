// import Swiper JS
import  Swiper, { Navigation, Pagination } from 'swiper'

Swiper.use([Navigation, Pagination])

// Get endpoint data with axios
const axios = require('axios')

axios
  .get('/wp-json/wc-challenge/team-info/')
  .then(function (response) {
    // handle success
    const slider = document.querySelector('.about-slider')

    response.data.forEach(function (team) {

      slider.innerHTML += `
                <div class="about-slider__card swiper-slide">
                    <div class="about-slider__card-header">
                        <img src="/wp-content/plugins/wc-challenge/public/images/member-${team.id}.png" alt="${team.name}" class="about-slider__card-img" onerror="this.onerror=null;this.src='/wp-content/plugins/wc-challenge/public/images/placeholder.png';">
                    </div>
                    <div class="about-slider__card-body">
                        <h2 class="about-slider__card-name">${team.name}</h2>
                        <p class="about-slider__card-position">${team.company.name}</p>
                    </div>
                </div>
            `
    })

    new Swiper('.slide-content', {
      slidesPerView: 4,
      spaceBetween: 47,
      loop: true,
      centerSlide: 'true',
      fade: 'true',
      grabCursor: 'true',
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        dynamicBullets: true
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        0: {
          slidesPerView: 1
        },
        520: {
          slidesPerView: 2
        },
        950: {
          slidesPerView: 3
        },
        1200: {
          slidesPerView: 4
        }
      }
    })
  })
  .catch(function (error) {
    // handle error
    console.log(error)
  })
