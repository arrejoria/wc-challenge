<?php
/**
 * Register API endpoint
 */
add_action( 'rest_api_init', function () {
	register_rest_route( 'wc-challenge', '/team-info/', array(
		'methods'  => 'GET',
		'callback' => 'wp_challenge_get_team_info',
	) );
} );

function wp_challenge_get_team_info( object $data ) {
    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', 'https://jsonplaceholder.typicode.com/users');
    $content = json_decode($res->getBody()->getContents());

    if ( $content ) {
        $response = new WP_REST_Response($content);
    } else {
        $response = new WP_Error(
            'no_data',
            'There is no valid data to display',
            array( 'status' => 404 ) );
    }

    return $response;
}