<?php

/**
 * Template used to render the team members page
 *
 */

?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>

</head>

<body>
    <section class="about">
        <div class="container">
            <div class="about-header">
                <div class="about-header__inner-text">
                    <h2 class="about-header__inner-name">About us</h2>
                    <h1 class="about-header__inner-title">Meet our <strong>team</strong></h1>
                </div>
                <div class="about-header__inner-img">
                    <img src="/wp-content/plugins/wc-challenge/public/images/group-18.svg" alt="pencil illustration" class="about-header__inner-img">
                </div>
            </div>
            <!-- Slider main container -->
            <div class="about-body swiper">
                <div class="slide-content">
                    <!-- Additional required wrapper -->
                    <div class="about-slider swiper-wrapper">
                        <!-- Slides -->
                    </div>

                    <!-- pagination -->
                    <div class="about-slider__pagination--hide swiper-pagination"></div>

                    <!-- navigation buttons -->
                    <div class="about-slider__buttons--hide swiper-button-prev"></div>
                    <div class="about-slider__buttons--hide swiper-button-next"></div>
                </div>
            </div>
    </section>
    </div>
    <section class="cta">
        <div class="cta-divider">
            <svg width="100%" height="32" viewBox="0 0 1449 32" fill="none" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" >
                <path d="M258.516 26.2132C133.71 43.9594 33.8653 13.3357 0 0.110161H1449C1410.78 11.0734 1344.59 33 1224.68 33C1074.8 33 998.859 -4.06628 848.98 15.772C699.101 35.6103 604.546 29.3456 509.992 15.772C415.437 2.19845 331.947 15.772 258.516 26.2132Z" fill="#F8F8F8" />
            </svg>
        </div>
        <div class="container">
            <div class="cta-content">
                <img src="/wp-content/plugins/wc-challenge/public/images/illustration.svg" alt="cup illustration" class="cta-content__image">
                <h1 class="cta-content__title">Make your next <strong>event better!</strong></h1>
                <p class="cta-content__description"><span>Book a Sketch Effect Live Artist</span> for your next virtual
                    meeting, event, or
                    webinar. </p>
            </div>
    </section>
    </div>

    <?php wp_footer(); ?>
</body>

</html>