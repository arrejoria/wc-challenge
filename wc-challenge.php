<?php
/**
* Plugin Name: wc-challenge
* Plugin URI: http://wc-challenge.es
* Description: white canvas challenge para el puesto de dev semi-senior.
* Version: 1.0.0
* Author: Arrejoria Lucas
* Author URI: http://arr-dev.vercel.app
* Requires at least: 4.0
* Tested up to: 4.3
*
* Text Domain: wc-challenge
* Domain path: /languages/
*/

defined( 'ABSPATH' ) or die( 'Bye bye' );
define('WC_PATH', plugin_dir_path(__FILE__));

/**
 * Load composer packages
 */
require( 'vendor/autoload.php' );

/**
 * Add plugin functions to init.
 */
add_action( 'init', function() {
	add_action( 'template_redirect',  'wc_challenge_template' );
	add_filter( 'rewrite_rules_array', 'wc_challenge_rule' );
	add_filter( 'query_vars', 'wc_challenge_query_vars' );
	add_action( 'wp_loaded', 'wc_challenge_flush_rules' );
}, 0 );

/**
* Add rewrite rule to detect the URL /team-members
*/
function wc_challenge_rule($rules ) {
    $newrules = array();
    $newrules['team-members'] = 'index.php?team_members=true';
    return $newrules + $rules;
}

/**
* Register the team_members query variable
*/
function wc_challenge_query_vars($vars) {
    $vars[] = 'team_members';
    return $vars;
}

/**
* Flush rewrite rules
*/
function wc_challenge_flush_rules(){
    $rules = get_option( 'rewrite_rules' );

    if ( ! isset( $rules['team-members'] ) ) {
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }
}

/**
* Load template if the team_members variable is present
*/
function wc_challenge_template() {
    if(get_query_var('team_members')) {
        add_filter('template_include', function() {
            return WC_PATH . '/public/template-team-members.php';
        });
    }
}

/**
 * Add styles and scripts to the template
 */
add_action( 'wp_enqueue_scripts', function() {
	if(get_query_var('team_members')) {
		wp_enqueue_style( 'wc_challenge_styles', plugin_dir_url( __FILE__ ) . 'public/css/main.min.css' );
		wp_enqueue_script( 'wc_challenge_scripts', plugin_dir_url( __FILE__ ) . 'public/js/app.min.js' );
	}
}, 99);

/**
 * Add styles and scripts to the template
 */
require( 'inc/team-info-api.php' );