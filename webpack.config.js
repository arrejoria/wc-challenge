const path = require( 'path' );

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: [
        './assets/styles/main.scss',
        './assets/scripts/app.js',
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'js/app.min.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "css/main.min.css",
        }),
    ],
};